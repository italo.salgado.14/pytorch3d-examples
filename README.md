# Pytorch3D

Este git muestra implementaciones relacionadas a Pytorch3D, enfocadas en sus básicos.
La instalación puede verla en: https://github.com/facebookresearch/pytorch3d/blob/main/INSTALL.md

```sh
conda create -n pytorch3d python=3.8
conda activate pytorch3d
conda install -c pytorch pytorch=1.7.1 torchvision cudatoolkit=10.2
conda install -c fvcore -c iopath -c conda-forge fvcore iopath

conda install -c bottler nvidiacub

conda install jupyter
pip install scikit-image matplotlib imageio plotly opencv-python

pip install black 'isort<5' flake8 flake8-bugbear flake8-comprehensions

conda install pytorch3d -c pytorch3d
```

## conda-cheatsheet.pdf

Un archivo muy util donde se especifica el manejo de los ambientes conda, necesarios para probar estos códigos.

## 01_deform

Deformación básica de un objeto importado tipo malla 3D.
